# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED=1

WORKDIR /django-image-hosting
COPY . /django-image-hosting/

RUN pip install -r requirements.txt

