from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from image.domain.entities import Image


class ImageSerializer(DataclassSerializer):
    image = serializers.ImageField(source='file')

    class Meta:
        fields = ['uuid', 'image']
        dataclass = Image


class ImageUUIDSerializer(DataclassSerializer):
    image_uuid = serializers.UUIDField(source='uuid')

    class Meta:
        dataclass = Image
        fields = ['image_uuid']


class ImageFileSerializer(DataclassSerializer):
    image = serializers.ImageField(source='file')

    class Meta:
        dataclass = Image
        fields = ['image']
