from rest_framework.routers import DefaultRouter

from image.api import ImageViewSet

router = DefaultRouter()
router.register(r'image', ImageViewSet, basename='image')

image_urlpatterns = router.urls
