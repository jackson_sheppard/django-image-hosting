from image.domain.entities import Image
from image.models import DBImage
from interfaces.builders import Builder, DestinationObj


class ImageWriteBuilder(Builder[Image, DBImage]):
    def build(self, db_image: DBImage) -> None:
        db_image.file = self._reference_data.file
        db_image.mime_type = self._reference_data.mime_type


class ImageReadBuilder(Builder[DBImage, Image]):
    def build(self, image: Image) -> None:
        image.file = self._reference_data.file
        image.mime_type = self._reference_data.mime_type
        image.uuid = self._reference_data.uuid
