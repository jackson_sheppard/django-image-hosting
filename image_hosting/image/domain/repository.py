import uuid as _uuid
from typing import Union

from image.domain.entities import Image
from image.models import DBImage
from interfaces.repository import Repository


class ImageRepository(Repository[Image]):
    @classmethod
    def _get_db_class(cls) -> DBImage:
        return DBImage

    @classmethod
    def _get_entity_class(cls) -> Image:
        return Image

    @classmethod
    def _set_pk(cls, entity: Image, db_instance: DBImage):
        entity.uuid = db_instance.uuid

    @classmethod
    def _get_pk(cls, entity: Image) -> Union[int, _uuid.UUID]:
        return entity.uuid
