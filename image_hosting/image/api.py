from django.http import HttpResponse
from rest_framework import viewsets, permissions
from rest_framework.response import Response

from image.domain.builders import ImageReadBuilder
from image.domain.repository import ImageRepository
from image.serializers import ImageUUIDSerializer, ImageSerializer


class ImageViewSet(viewsets.ViewSet):
    permission_classes = [permissions.AllowAny, ]

    def list(self, request, pk=None):
        all_images = ImageRepository.get_all(filters=[], builders=[ImageReadBuilder])

        return Response(ImageSerializer(all_images, many=True).data)

    def retrieve(self, request, pk=None):
        img_ser = ImageUUIDSerializer(data={'image_uuid': pk})
        img_ser.is_valid(raise_exception=True)
        image = img_ser.validated_data

        ImageRepository.get_by_pk(entity=image, builders=[ImageReadBuilder])

        return HttpResponse(image.file.read(), content_type=image.mime_type)
