from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from upload.domain.entities import Upload


class UploadURLSerializer(DataclassSerializer):
    url = serializers.URLField(source='source_url')

    class Meta:
        dataclass = Upload
        fields = ['url']



