from rest_framework.routers import DefaultRouter

from image_upload.api import ImageUploadViewset

router = DefaultRouter()
router.register(r'image_upload', ImageUploadViewset, basename='image_upload')

image_upload_urlpatterns = router.urls
