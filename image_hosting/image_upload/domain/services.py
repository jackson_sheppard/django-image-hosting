import mimetypes
from io import BytesIO

import uuid as _uuid
from django.core.files import File
from image.domain.builders import ImageWriteBuilder, ImageReadBuilder
from image.domain.entities import Image
from image.domain.repository import ImageRepository
from upload.domain.entities import Upload
from upload.domain.services import UploadService


class UploadDataIsNone(Exception):
    pass


class ImageUploadService:

    @classmethod
    def create_image_from_upload(cls, upload: Upload) -> Image:
        upload = UploadService.get_from_url(url=upload.source_url)

        if upload.data is None:
            raise UploadDataIsNone()

        file = File(file=BytesIO(upload.data), name=f"{_uuid.uuid4()}{mimetypes.guess_extension(upload.mime_type)}")

        new_image = Image(file=file, mime_type=upload.mime_type)

        ImageRepository.save(entity=new_image, builders=[ImageWriteBuilder])

        ImageRepository.get_by_pk(entity=new_image, builders=[ImageReadBuilder])

        return new_image