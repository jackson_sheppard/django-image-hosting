from .settings import *


STATIC_URL = "/static/"
MEDIA_URL = "/"

STATICFILES_DIRS = [
    BASE_DIR / 'static'
]

