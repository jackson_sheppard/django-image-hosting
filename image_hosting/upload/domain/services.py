import mimetypes
import urllib.request

from upload.domain.entities import Upload


class UploadService:

    @classmethod
    def get_from_url(cls, url: str) -> Upload:
        received_file = Upload(mime_type=mimetypes.guess_type(url)[0])

        req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})

        with urllib.request.urlopen(req) as r:
            received_file.data = r.read()

        return received_file
